package project.plugIn;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import project.explorer.ModeleTypeFile;

public class EditeurPlugIn extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Container pane;
	
	private JPanel topPanel;
	private JPanel panelPrincipal;
	private JPanel zoneOutils;
	
	public static ModeleTypeFile modelFile;
	public static JTable tableFiles;
	public static JScrollPane jscpLivre = new JScrollPane();

	
	public EditeurPlugIn() {

		setTitle("Menu Plug-In");
		setSize(1300, 800);
		setBackground(Color.GREEN);
		
		pane = getContentPane();
		topPanel = new JPanel();
		topPanel.setLayout( new BorderLayout() );
		pane.add(topPanel);
		
		
		afficherListePlugInDispo();
		afficherOutils();
		
		topPanel.add(panelPrincipal, BorderLayout.EAST);
		topPanel.add(zoneOutils, BorderLayout.WEST);
	}

	

	private void afficherListePlugInDispo() {

		panelPrincipal = new JPanel();
	}
	
	private void afficherOutils() {

		zoneOutils = new JPanel();

		JButton boutonAddPlugIn = new JButton("Ajouter un Plug-in");
		zoneOutils.add(BorderLayout.WEST, boutonAddPlugIn);
		
		
	}
	
	

}
