package project.explorer;

import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

public class ModeleTypeFile extends AbstractTableModel {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String[] columnNames =  { "Type objet","Objet Trouv�"};
	private ArrayList<ArrayList<Object>> data ;
	
	public ModeleTypeFile(){
		data = new ArrayList<ArrayList<Object>>();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public int getRowCount(){
		return data.size();
	}
	
	public String getColumnName(int col) {
	    return columnNames[col];
	}
	
	public Object getValueAt(int row, int col) {
	    return data.get(row).get(col);
	}
	
	/*
	 * JTable uses this method to determine the default renderer/
	 * editor for each cell.  If we didn't implement this method,
	 * then the last column would contain text ("true"/"false"),
	 * rather than a check box.
	 */
	
	public Class getColumnClass(int c) {
	    return getValueAt(0, c).getClass();
	}
	
	public boolean isCellEditable(int row, int col) {
	    return false;
	}
	
	public void addRow(String nom, String valeur, final JScrollPane pane) {
				
		ArrayList<Object> element = new ArrayList<Object>();
		element.add(nom);
		element.add(valeur);		
		
		data.add(element);
		
		miseAJour(pane);
	}

	public void miseAJourRow(int row, String nom, String valeur, final JScrollPane pane) {
			
		data.remove(row);
		
		ArrayList<Object> element = new ArrayList<Object>();
		element.add(nom);
		element.add(valeur);
		
		data.add(row,element);
		
		miseAJour(pane);
	}
	
	 public void removeRow(int rowIndex, final JScrollPane pane) {
	        data.remove(rowIndex);
	 
	        miseAJour(pane);
	        //fireTableRowsDeleted(rowIndex, rowIndex);
	    }



	private void miseAJour( final JScrollPane pane) {
		fireTableDataChanged();
	}

}