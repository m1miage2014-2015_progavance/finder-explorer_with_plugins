package project.explorer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import project.plugIn.EditeurPlugIn;

public class EditeurVisitor extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Container pane;
	
	private JPanel topPanel;
	private JPanel panelPrincipal;
	private JPanel zoneOutils;
	
	public static ModeleTypeFile modelFile;
	public static JTable tableFiles;
	public static JScrollPane jscpLivre = new JScrollPane();
	
	public String repertoirePrecedent;
	public String repertoireCourant;
	
	ActionListener changeRep = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {

			System.out.println("action demand�e : " + e.getActionCommand());
			
			if(e.getActionCommand().equals("nouveauRep")){
				
				// on r�cup�re les valeurs de la ligne s�lectionn�e puis traitement (rentrer dans le dir ou ouvrir le fichier)
				if(tableFiles.getSelectedRow() >= 0){
					
					int ligneSelectionnee = tableFiles.getSelectedRow();
					
					if(tableFiles.getValueAt(ligneSelectionnee, 0).equals("R�pertoire")){
						
						String repertoireAVisiter = (String) tableFiles.getValueAt(ligneSelectionnee, 1);
						System.out.println(repertoireAVisiter);
						
						repertoirePrecedent = repertoireCourant;
						repertoireCourant = repertoireAVisiter;
						
						topPanel.removeAll();
						
						afficherListeFiles();
						afficherOutils();
						
						topPanel.add(panelPrincipal, BorderLayout.EAST);
						topPanel.add(zoneOutils, BorderLayout.WEST);

						topPanel.repaint();
						topPanel.validate();
						
					} else if(tableFiles.getValueAt(ligneSelectionnee, 0).equals("Fichier")){
						
//						TODO ouvrir le fichier
						String fichierAOuvrir = (String) tableFiles.getValueAt(ligneSelectionnee, 1);
						System.out.println("Fichier � ouvrir : " + fichierAOuvrir);
					}
					
				}
				
			} else if(e.getActionCommand().equals("ancienRep")){
				System.out.println("retour au repertoire pr�c�dent : " + repertoirePrecedent);

				
				repertoireCourant = repertoirePrecedent;
				repertoirePrecedent = repertoirePrecedent+"/..";
				
				topPanel.removeAll();
				
				afficherListeFiles();
				afficherOutils();
				
				topPanel.add(panelPrincipal, BorderLayout.EAST);
				topPanel.add(zoneOutils, BorderLayout.WEST);

				topPanel.repaint();
				topPanel.validate();
				
			} else if(e.getActionCommand().equals("menuPlugIn")){
				
				EditeurPlugIn menuPlugIn = new EditeurPlugIn();
				menuPlugIn.setVisible(true);
			}
		}
	};

	
	public EditeurVisitor() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Visitor");
		setSize(1300, 800);
		setBackground(Color.GREEN);
		
		pane = getContentPane();
		topPanel = new JPanel();
		topPanel.setLayout( new BorderLayout() );
		pane.add(topPanel);
		
		// r�pertoire dans lequel on rentre par d�faut
		this.repertoireCourant = "C:/";
		this.repertoirePrecedent = "C:/";
		
		afficherListeFiles();
		afficherOutils();
		
		topPanel.add(panelPrincipal, BorderLayout.EAST);
		topPanel.add(zoneOutils, BorderLayout.WEST);
	}

	

	private void afficherListeFiles() {

		panelPrincipal = new JPanel();

		Dimension dimJscp = new Dimension(600, 400);
		
		/*
		 * Creation de la table
		 */
		JPanel panelListeLivre = new JPanel();
		panelListeLivre.setBorder(BorderFactory.createTitledBorder("Fichier / R�pertoire  :"));

		
		modelFile = new ModeleTypeFile();
		tableFiles = new JTable(modelFile);
		jscpLivre.setViewportView(tableFiles);
		jscpLivre.setSize(dimJscp);
		jscpLivre.setPreferredSize(dimJscp);
		jscpLivre.setMaximumSize(dimJscp);
		jscpLivre.setMinimumSize(dimJscp);
		
		Visitor visite = new Visitor();
		
		/*
		 * 	R�cup�re les fichiers pr�sents pour le r�pertoire donn�e
		 */
		Path path = new File(this.repertoireCourant).toPath();

		HashMap<Path, String> listePathTrouve = visite.visitDirectory(path);
		
		/* r�cup�re les valeurs de la hash Map */
		for (Path clef : listePathTrouve.keySet()) {
			// utilise ici hashMap.get(mapKey) pour acc�der aux valeurs
			String valeur = listePathTrouve.get(clef);
			 modelFile.addRow(valeur , clef.toString(), jscpLivre);
		}

		panelListeLivre.add(jscpLivre);
		panelPrincipal.add(panelListeLivre);
		
	}
	
	private void afficherOutils() {

		zoneOutils = new JPanel();
		
		JButton boutonRetourArriere = new JButton("Reculer");
		zoneOutils.add(BorderLayout.WEST, boutonRetourArriere);

		JButton boutonDossierSuivant = new JButton("Avancer");
		zoneOutils.add(BorderLayout.CENTER, boutonDossierSuivant);
		
		JButton boutonMenuPlugIn = new JButton("Menu Plug-In");
		zoneOutils.add(BorderLayout.EAST, boutonMenuPlugIn);
		
		boutonRetourArriere.setActionCommand("ancienRep");
		boutonRetourArriere.addActionListener(this.changeRep);
		
		boutonDossierSuivant.setActionCommand("nouveauRep");
		boutonDossierSuivant.addActionListener(this.changeRep);
		
		boutonMenuPlugIn.setActionCommand("menuPlugIn");
		boutonMenuPlugIn.addActionListener(this.changeRep);
		
	}
	
}
