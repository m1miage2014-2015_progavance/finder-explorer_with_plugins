package project.explorer;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;

public class Visitor {

	/**
	 * M�thode visitant un r�pertoire
	 * @param dir = r�pertoire dans lequel on souhaite rentrer
	 * @return HashMap<Path, String> = une HashMap, diff�renciant les fichiers des repertoires contenus dans le dir visit�
	 * @autor R�mi ALLEGRO
	 */
	public HashMap<Path, String> visitDirectory(Path dir){

		final HashMap<Path, String> listePathTrouves = new HashMap<Path, String>();
		

		File repertoire = new File(dir.toString());
		String [] listefichiers = repertoire.list();
		System.out.println("On trouve dans " + repertoire.getAbsolutePath() + " ; les Dossiers/Fichiers : ");
		for(int i = 0; i <listefichiers.length; i++){
			
			// on v�rifie si le fichier trouv� est un r�pertoire ou non
			
			File fichierTrouve = new File(repertoire.getAbsolutePath()+File.separator+listefichiers[i]);
			
			Path pathDufichierTrouve = fichierTrouve.toPath();
			
			if(fichierTrouve.exists()){
				
				if(fichierTrouve.isDirectory()){
					System.out.println("je suis un r�pertoire : " + fichierTrouve.toString());
					listePathTrouves.put(pathDufichierTrouve, "R�pertoire");
				} else if(fichierTrouve.isFile()){
					System.out.println("je suis un fichier : " + fichierTrouve.toString());
					listePathTrouves.put(pathDufichierTrouve, "Fichier");
				}
			} else {
				// le fichier n'est pas reconnu comme existant, il n'est donc pas ajout� � la HashMap
				System.out.println("j'existe pas : " + fichierTrouve.toString());
			}
			
			
		}
		
//		final Path repertoire = dir;
//
//		try {
//			
//			Files.walkFileTree(repertoire, new SimpleFileVisitor<Path>() {
//
//
//				@Override
//				public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
//					
//					listePathTrouves.put(file, "Fichier");
//					
//					return FileVisitResult.CONTINUE;
//					
//				}
//
//				@Override
//				public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
//
////					if(dir.toFile().isDirectory()){
////						System.out.println("je suis un r�pertoire : " + dir.toString());
////					} else {
////						System.out.println("je suis un fichier : " + dir.toString());
////					}
//					
//					DirectoryStream.Filter<Path> filtre = new DirectoryStream.Filter<Path>() {
//
//						@Override
//						public boolean accept(Path entry) throws IOException {
//
//							return true;
//						}
//					};
//
//					try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filtre)) {
//						for (Path entry : stream) {
//							
//							listePathTrouves.put(entry, "R�pertoire");
//						}
//					}
//
//					return FileVisitResult.CONTINUE;
//				}
//			});
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		return listePathTrouves;
	}
	
	
}
