README



Au lancement de l'application le programme crée les dossiers qu'il utilisera pour travailler:

-Windows :
    C:\Users\ "l'utilisateur" \Desktop\FinderExplorerWithPlugin
-Linux (Distribution Ubuntu):
    /home/ "l'utilisateur" /FinderExplorerWithPlugin
-Mac OS X :
    /Users/ "l'utilisateur" /FinderExplorerWithPlugin
    
Le dossier SerializedFiles, contiendra les fichiers de sauvegarde.
Le dossier plugins, contiendra les .jar des plugins.